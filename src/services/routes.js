import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { getToken } from './browserStorage';
import { PATHS } from '../constants';
import { App, LoanAppDetails, SignUp, NotFoundPage, PageLoader } from '../containers';

const PublicRoute = ({ component: Component, ...otherProps }) => (
    <Route
        {...otherProps}
        render={props => {
            // if (getToken()) {
            //     return <Redirect to={PATHS.lonAppDetails.path} />;
            // }
            return <Component {...props} />;
        }}
    />
);

const AppMainLayout = () => {
    if (getToken()) {
        return (
            <div id="lba-app-layout">
                <App>
                    <Switch>
                        <Route exact path={PATHS.lonAppDetails.path} component={LoanAppDetails} />
                        <Route component={NotFoundPage} />
                    </Switch>
                </App>
            </div>
        );
    }

    // TODO: TBD
    return <Route component={NotFoundPage} />;
    // return <Redirect to={`${PATHS.login.path}?next=${encodeURIComponent(window.location.pathname)}`} />;
};

export const Routes = () => (
    <div id="lba-routes">
        <PageLoader />
        <Switch>
            <PublicRoute path={PATHS.signUp.path} component={SignUp} />
            <Route path={PATHS.lonAppDetails.path} component={AppMainLayout} />
        </Switch>
    </div>
);
