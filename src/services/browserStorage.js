const TOKEN_KEY = 'lba_auth_token';

export function getToken() {
    return sessionStorage.getItem(TOKEN_KEY) || true; // TODO: Remove after auth integration
}

export function setToken(token) {
    sessionStorage.setItem(TOKEN_KEY, token);
}

export function clearToken() {
    sessionStorage.removeItem(TOKEN_KEY);
}
