// import Axios from 'axios';
// import { SESSION_API_URL } from '../../constants';

// TODO: TBD

export function create(userData) {
    // return Axios.post(`${SESSION_API_URL}/user`, userData);

    return new Promise(resolve => {
        setTimeout(() => resolve({ data: userData }), 1000);
    });
}

export function login(credentials) {
    // return Axios.post(`${SESSION_API_URL}/user/login`, credentials);

    return new Promise(resolve => {
        setTimeout(
            () =>
                resolve({
                    data: {
                        user: {
                            id: 0,
                            firstName: 'string',
                            lastName: 'string',
                            email: 'string'
                        },
                        authentication: {
                            authenticationToken: 'DC124947VDE435FVA&23',
                            expiresAt: 'UTC timestamp'
                        }
                    }
                }),
            1000
        );
    });
}

export function logout() {
    // return Axios.get(`${SESSION_API_URL}/user/logout`);

    return new Promise(resolve => {
        setTimeout(() => resolve({ data: {} }), 1000);
    });
}

export function getUserData() {
    // return Axios.get(`${SESSION_API_URL}/user/:id`);

    return new Promise(resolve => {
        setTimeout(
            () =>
                resolve({
                    data: {
                        id: 0,
                        firstName: 'Dima',
                        lastName: 'Poluyan',
                        email: 'test@test.com'
                    }
                }),
            1000
        );
    });
}
