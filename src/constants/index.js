export const PATHS = {
    lonAppDetails: {
        id: '',
        path: '/'
    },
    signUp: {
        id: 'sign-up',
        path: '/sign-up'
    }
};

export const SESSION_API_URL = '/api/v1';

export const KEYBOARD_KEY_VALUES = {
    ENTER: 'Enter',
    ESCAPE: 'Escape',
    ARROW_LEFT: 'ArrowLeft',
    ARROW_RIGHT: 'ArrowRight',
    ARROW_DOWN: 'ArrowDown',
    ARROW_UP: 'ArrowUp',
    HOME: 'Home',
    END: 'End'
};
