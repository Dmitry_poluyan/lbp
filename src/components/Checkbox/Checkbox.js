import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './Checkbox.css';

class Checkbox extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            checked: props.checkedDefault,
            hasFocus: false
        };

        this.handleFocus = this.handleFocus.bind(this);
        this.handleBlur = this.handleBlur.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.setState({ checked: event.target.checked });
        this.props.onChange(event);
    }

    handleFocus(event) {
        this.setState({ hasFocus: true });
        this.props.onFocus(event);
    }

    handleBlur(event) {
        this.setState({ hasFocus: false });
        this.props.onBlur(event);
    }

    render() {
        const { className, name, value, error, label, required, disabled, helpText, checked: propChecked } = this.props;
        const { checked, hasFocus } = { ...this.state, checked: propChecked };

        const classes = classNames(
            'lba-checkbox',
            hasFocus && !disabled && 'lba-checkbox--has-focus',
            required && !disabled && 'lba-checkbox--required',
            disabled && 'lba-checkbox--disabled',
            className
        );

        return (
            <div className={classes}>
                <label className="lba-checkbox-wrapper">
                    <input
                        className="lba-checkbox-native-control"
                        name={name}
                        type="checkbox"
                        value={value}
                        disabled={disabled}
                        checked={checked}
                        onFocus={this.handleFocus}
                        onBlur={this.handleBlur}
                        onChange={this.handleChange}
                    />
                    <span className="lba-checkbox-control" aria-hidden />
                    {label && <span className="lba-checkbox-label">{label}</span>}
                </label>
                {error && (
                    <div role="alert" className="lba-checkbox-error" aria-live="polite">
                        {error}
                    </div>
                )}
                {helpText && <small className="lba-checkbox-help-text">{helpText}</small>}
            </div>
        );
    }
}

Checkbox.propTypes = {
    className: PropTypes.string,
    name: PropTypes.string.isRequired,
    id: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.bool]),
    checked: PropTypes.bool,
    checkedDefault: PropTypes.bool,
    label: PropTypes.node,
    error: PropTypes.node,
    helpText: PropTypes.node,
    required: PropTypes.bool,
    disabled: PropTypes.bool,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    onChange: PropTypes.func
};

Checkbox.defaultProps = {
    disabled: false,
    checkedDefault: false,
    onFocus: () => {},
    onBlur: () => {},
    onChange: () => {}
};

export default Checkbox;
