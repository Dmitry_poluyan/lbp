import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';

import Checkbox from '../Checkbox';

function renderComponent({ id = 'test', name = 'test', ...otherProps } = {}, mountFn = shallow) {
    return mountFn(<Checkbox id={id} name={name} {...otherProps} />);
}

describe('<Checkbox /> component', () => {
    it('should render with necessary elements', () => {
        const checkbox = renderComponent({ label: 'Test' });

        expect(checkbox.find('.lba-checkbox')).toHaveLength(1);
        expect(checkbox.find('input[type="checkbox"].lba-checkbox-native-control')).toHaveLength(1);
        expect(checkbox.find('.lba-checkbox-control')).toHaveLength(1);
        expect(checkbox.contains(<span className="lba-checkbox-label">Test</span>)).toBeTruthy();
        expect(checkbox.find('input.lba-checkbox-native-control').props().disabled).toBeFalsy();

        const dom = renderer
            .create(
                <Checkbox name="feature" label="Claws" required helpText="Lorem" error="Please accept the terms." />
            )
            .toJSON();
        expect(dom).toMatchSnapshot();
    });

    it('should render with help text', () => {
        const checkbox = renderComponent({ helpText: 'lorem ipsum' });

        expect(checkbox.contains(<small className="lba-checkbox-help-text">lorem ipsum</small>)).toBeTruthy();
    });

    it('should render with error', () => {
        const checkbox = renderComponent({ error: 'this is required field' });

        expect(
            checkbox.contains(
                <div role="alert" className="lba-checkbox-error" aria-live="polite">
                    this is required field
                </div>
            )
        ).toBeTruthy();
    });

    it('should render with necessary class if checkbox is required', () => {
        const checkbox = renderComponent({ label: 'Required', required: true });

        expect(checkbox.hasClass('lba-checkbox--required')).toBeTruthy();
    });

    it('should render with necessary class if checkbox is disabled', () => {
        const checkbox = renderComponent({ label: 'Disabled', required: true, disabled: true });

        expect(checkbox.hasClass('lba-checkbox--disabled')).toBeTruthy();
        expect(checkbox.hasClass('lba-checkbox--required')).toBeFalsy();
        expect(checkbox.find('input.lba-checkbox-native-control').props().disabled).toBeTruthy();
    });

    it('should not throw an error if `onFocus` property is not given', () => {
        const checkbox = renderComponent();

        expect(() => {
            checkbox.find('.lba-checkbox-native-control').simulate('focus', new FocusEvent('focus'));
        }).not.toThrow();
    });

    it('should handle focus event', () => {
        const onFocus = jest.fn();
        const event = new FocusEvent('focus');
        const checkbox = renderComponent({ onFocus });
        checkbox.find('.lba-checkbox-native-control').simulate('focus', event);
        checkbox.update();

        expect(checkbox.hasClass('lba-checkbox--has-focus')).toBeTruthy();
        expect(onFocus).toHaveBeenCalledWith(event);
    });

    it('should not throw an error if `onBlur` property is not given', () => {
        const checkbox = renderComponent();

        expect(() => {
            checkbox.find('.lba-checkbox-native-control').simulate('blur', new FocusEvent('blur'));
        }).not.toThrow();
    });

    it('should handle blur event', () => {
        const onBlur = jest.fn();
        const event = new FocusEvent('blur');
        const checkbox = renderComponent({ onBlur });
        checkbox.setState({ hasFocus: true });
        checkbox.update();
        checkbox.find('.lba-checkbox-native-control').simulate('blur', event);
        checkbox.update();

        expect(checkbox.hasClass('lba-checkbox--has-focus')).toBeFalsy();
        expect(onBlur).toHaveBeenCalledWith(event);
    });

    it('should not throw an error if `onChange` property is not given', () => {
        const checkbox = renderComponent();

        expect(() => {
            checkbox.instance().props.onChange(new Event('change'));
        }).not.toThrow();
    });

    it("should update component's state and call `onChange` callback", () => {
        const onChangeStub = jest.fn();
        const eventStub = { target: { name: 'test', checked: true } };
        const checkbox = renderComponent({ onChange: onChangeStub });

        checkbox.find('.lba-checkbox-native-control').simulate('change', eventStub);
        checkbox.update();

        expect(checkbox.instance().state.checked).toBeTruthy();
        expect(onChangeStub).toHaveBeenCalledWith(eventStub);
    });
});
