import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';

import Comment from '../Comment';

const defaultProps = {
    author: 'Michael Brown',
    message: "We think that it's difficult to afford a property of 50,000 € in Berlin with a down payment of 10,000 €."
};

function renderComponent(props = {}, mountFn = shallow) {
    return mountFn(<Comment {...defaultProps} {...props} />);
}

describe('<Comment /> Component', () => {
    it('should renders component', () => {
        const component = renderComponent();

        expect(component.hasClass('lba-comment')).toBeTruthy();
        expect(component.find('.lba-comment-author')).toHaveLength(1);
        expect(component.find('.lba-comment-text')).toHaveLength(1);
        expect(component.find('i.zmdi-comment-alt')).toHaveLength(1);

        const dom = renderer.create(<Comment {...defaultProps} />).toJSON();
        expect(dom).toMatchSnapshot();
    });

    it('should renders elements with correct content', () => {
        const component = renderComponent();

        expect(component.find('.lba-comment-author').text()).toBe('Michael Brown');
        expect(component.find('.lba-comment-text').text()).toBe(
            "We think that it's difficult to afford a property of 50,000 € in Berlin with a down payment of 10,000 €."
        );
    });

    it('should renders with dark theme', () => {
        const component = renderComponent({ theme: 'dark' });
        expect(component.hasClass('lba-comment')).toBeTruthy();
        expect(component.hasClass('lba-comment--dark')).toBeTruthy();
    });
});
