import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

import './Comment.css';

const Comment = ({ className, message, author, theme }) => {
    const classes = classNames('lba-comment', theme && `lba-comment--${theme}`, className);

    return (
        <div className={classes}>
            <div className="lba-comment-author">
                <i className="zmdi zmdi-comment-alt" />
                <p>{author}</p>
            </div>
            <div className="lba-comment-text">
                <p>{message}</p>
            </div>
        </div>
    );
};

Comment.propTypes = {
    className: PropTypes.string,
    theme: PropTypes.oneOf(['dark']),
    author: PropTypes.string,
    message: PropTypes.string
};

export default Comment;
