#### Comment component:

```jsx
const props = {
    author: 'Michael Brown',
    message: "We think that it's difficult to afford a property of 50,000 € in Berlin with a down payment of 10,000 €."
};

<Comment {...props} />
```

#### Comment component with dark theme:

```jsx
const wrapperStyles = {
    padding: '10px',
    background: 'linear-gradient(0deg, #0066B3 0%, #3F98D4 100%)'
};

const props = {
    author: 'Michael Brown',
    message: "We think that it's difficult to afford a property of 50,000 € in Berlin with a down payment of 10,000 €.",
    theme: 'dark'
};

<div style={wrapperStyles}>
    <Comment {...props} />
</div> 
```
