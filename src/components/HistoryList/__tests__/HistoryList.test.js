import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';

import HistoryList from '../HistoryList';

const defaultProps = {
    state: 'waiting',
    date: '1 March 2019',
    time: '5:01 PM',
    labels: {
        currency: '€',
        eventTitle: 'Application submitted'
    }
};

function renderComponent(props = {}, mountFn = shallow) {
    return mountFn(<HistoryList {...defaultProps} {...props} />);
}

xdescribe('<HistoryList /> Component', () => {
    it('should renders component', () => {
        const component = renderComponent();

        expect(component.hasClass('lba-history-list')).toBeTruthy();

        const dom = renderer.create(<HistoryList {...defaultProps} />).toJSON();
        expect(dom).toMatchSnapshot();
    });
});
