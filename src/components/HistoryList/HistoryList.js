import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

import HistoryItem from '../HistoryItem';
import './HistoryList.css';

const historyList = [
    {
        id: 1,
        state: 'review',
        date: '6 March 2019',
        time: '5:01 PM',
        labels: {
            eventTitle: 'Bank#1. Application approved.'
        },
        theme: 'dark'
    },
    {
        id: 2,
        state: 'review',
        date: '3 March 2019',
        time: '2:20 PM',
        amount: {
            prev: '50,000',
            new: '45,000'
        },
        labels: {
            currency: '€',
            eventTitle: 'Change lon application',
            amountTitle: 'Loan amount'
        },
        comment: {
            author: 'Michael Brown',
            message:
                "We think that it's difficult to afford a property of 50,000 € in Berlin with a down payment of 10,000 €."
        }
    },
    {
        id: 3,
        state: 'review',
        date: '3 March 2019',
        time: '2:01 PM',
        labels: {
            eventTitle: 'Application viewed'
        }
    },
    {
        id: 4,
        state: 'waiting',
        date: '1 March 2019',
        time: '5:01 PM',
        labels: {
            eventTitle: 'Application submitted'
        }
    }
];

const HistoryList = ({ className }) => {
    const classes = classNames('lba-history', className);

    return (
        <div className={classes}>
            <h1>History</h1>
            <div className="lba-history-list">
                {historyList.map(h => (
                    <HistoryItem {...h} key={h.id} />
                ))}
            </div>
        </div>
    );
};

HistoryList.propTypes = {
    className: PropTypes.string
};

HistoryList.defaultProps = {
    labels: {}
};

export default HistoryList;
