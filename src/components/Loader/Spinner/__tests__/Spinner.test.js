import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';

import Spinner from '../Spinner';

function renderComponent(props = {}, mountFn = shallow) {
    return mountFn(<Spinner {...props} />);
}

describe('<Spinner /> component', () => {
    it('should renders without errors', () => {
        renderComponent();

        const dom = renderer.create(<Spinner size="sm" />).toJSON();
        expect(dom).toMatchSnapshot();
    });

    it('should renders with different sizes', () => {
        let spinner = renderComponent();
        expect(spinner.hasClass('lba-spinner--size-md')).toBeTruthy();

        spinner = renderComponent({ size: 'sm' });
        expect(spinner.hasClass('lba-spinner--size-sm')).toBeTruthy();

        spinner = renderComponent({ size: 'lg' });
        expect(spinner.hasClass('lba-spinner--size-lg')).toBeTruthy();
    });
});
