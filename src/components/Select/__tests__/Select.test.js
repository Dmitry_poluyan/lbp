import React from 'react';
import { mount } from 'enzyme';
import renderer from 'react-test-renderer';

import Select from '../Select';
import { KEYBOARD_KEY_VALUES } from '../../../constants';

const defaultProps = {
    id: '1',
    options: [
        { value: '1 Person - 1600 kWh/Jahr', label: '1 Person - 1600 kWh/Jahr' },
        { value: '2 Personen - 2400 kWh/Jahr', label: '2 Personen - 2400 kWh/Jahr' },
        { value: '3 Personen - 3200 kWh/Jahr', label: '3 Personen - 3200 kWh/Jahr' },
        { value: '4 Personen - 4200 kWh/Jahr', label: '4 Personen - 4200 kWh/Jahr' }
    ]
};

function renderComponent(props = { ...defaultProps }, mountFn = mount) {
    return mountFn(<Select {...props} />);
}

describe('<Select /> Component', () => {
    it('should renders without errors', () => {
        const component = renderComponent();
        expect(component.find('input')).toHaveLength(1);
        expect(component.find('svg')).toHaveLength(1);
        expect(component.find('.lba-select-option-icons')).toHaveLength(0);
        expect(component.find('.lba-select-option--icon')).toHaveLength(0);
        expect(component.find('svg[role="button"]')).toHaveLength(1);
        expect(component.find('ul[role="listbox"]')).toHaveLength(1);

        const dom = renderer.create(<Select {...defaultProps} />).toJSON();
        expect(dom).toMatchSnapshot();

        expect(component.find('.lba-select-option')).toHaveLength(4);

        component.setProps({ autoComplete: true, editable: true });
        component.update();
        expect(component.find('.lba-select-option')).toHaveLength(4);

        component.setProps({ value: '1600' });
        component.update();
        expect(component.find('.lba-select-option')).toHaveLength(1);

        component.setProps({ value: '1 person - 1600 kwh/jahr' });
        component.update();
        expect(component.find('.lba-select-option')).toHaveLength(1);
    });

    it('should update component state, if some option was chosen', () => {
        const component = renderComponent();

        component.setState({ expanded: true });
        component
            .find('.lba-select-option')
            .at(2)
            .simulate('click');
        component.update();

        expect(component.state().value).toEqual('3 Personen - 3200 kWh/Jahr');
    });

    it('should invoke onChange callback, if some option was chosen', () => {
        const propsMock = {
            options: [
                { value: '1600', label: '1 Person - 1600 kWh/Jahr ' },
                { value: '2400', label: '2 Personen - 2400 kWh/Jahr' },
                { value: '3200', label: '3 Personen - 3200 kWh/Jahr' }
            ],
            onChange: jest.fn()
        };

        const component = renderComponent(propsMock);

        component.setState({ expanded: true });
        component
            .find('.lba-select-option')
            .at(2)
            .simulate('click');
        component.update();

        expect(propsMock.onChange).toHaveBeenCalledWith({ name: 'consumptionSelect', value: '3200' });
    });

    it('should call "onChange" and "onBlur" methods when key down ENTER on option', () => {
        const propsMock = {
            options: [
                { value: '1600', label: '1 Person - 1600 kWh/Jahr ' },
                { value: '2400', label: '2 Personen - 2400 kWh/Jahr' },
                { value: '3200', label: '3 Personen - 3200 kWh/Jahr' }
            ],
            onChange: jest.fn(),
            onBlur: jest.fn()
        };

        const component = renderComponent(propsMock);

        component.setState({ expanded: true });
        component
            .find('.lba-select-option')
            .at(1)
            .simulate('keyup', { key: KEYBOARD_KEY_VALUES.ENTER });
        component.update();

        expect(propsMock.onChange).toHaveBeenCalledWith({ name: 'consumptionSelect', value: '2400' });
        expect(propsMock.onBlur).toHaveBeenCalledWith({ target: { name: 'consumptionSelect', value: '2400' } });
    });

    it('should open options after alt + arrow down combination for auto complete select, navigate between options by arrow up/down', () => {
        const props = {
            options: [
                { value: '1600', label: '1 Person - 1600 kWh/Jahr ' },
                { value: '2400', label: '2 Personen - 2400 kWh/Jahr' },
                { value: '3200', label: '3 Personen - 3200 kWh/Jahr' }
            ],
            autoComplete: true,
            editable: true,
            onChange: jest.fn(),
            onBlur: jest.fn()
        };

        const component = renderComponent(props);
        component.instance().optionRef.focus = jest.fn();

        component.setState({ expanded: false });
        component
            .find('.lba-select')
            .at(0)
            .simulate('keydown', { key: KEYBOARD_KEY_VALUES.ARROW_DOWN, altKey: true });
        component.update();
        expect(component.state()).toEqual({ expanded: true, value: '' });
        expect(component.instance().optionRef.focus).toHaveBeenCalledTimes(0);
        component
            .find('.lba-select')
            .at(0)
            .simulate('keydown', { key: KEYBOARD_KEY_VALUES.ARROW_DOWN, altKey: true });
        component.update();
        expect(component.state()).toEqual({ expanded: false, value: '' });
        expect(component.instance().optionRef.focus).toHaveBeenCalledTimes(0);

        component
            .find('.lba-select')
            .at(0)
            .simulate('keydown', { key: KEYBOARD_KEY_VALUES.ARROW_DOWN, altKey: true });
        component.update();
        component
            .find('.lba-select')
            .at(0)
            .simulate('keydown', { key: KEYBOARD_KEY_VALUES.ARROW_DOWN });
        component.update();
        expect(component.state()).toEqual({ expanded: true, value: '' });
        expect(component.instance().optionRef.focus).toHaveBeenCalledTimes(1);

        const nextSibling = { focus: jest.fn() };
        const previousSibling = { focus: jest.fn() };
        component
            .find('.lba-select-option')
            .at(0)
            .simulate('keyup', {
                key: KEYBOARD_KEY_VALUES.ARROW_DOWN,
                target: {
                    nextSibling,
                    previousSibling
                }
            });
        component.update();
        expect(nextSibling.focus).toHaveBeenCalledTimes(1);
        expect(previousSibling.focus).toHaveBeenCalledTimes(0);

        component
            .find('.lba-select-option')
            .at(1)
            .simulate('keyup', {
                key: KEYBOARD_KEY_VALUES.ARROW_UP,
                target: {
                    nextSibling,
                    previousSibling
                }
            });
        component.update();
        expect(nextSibling.focus).toHaveBeenCalledTimes(1);
        expect(previousSibling.focus).toHaveBeenCalledTimes(1);
    });

    it('should call "onChange" and "onBlur" methods when click on option', () => {
        const propsMock = {
            name: 'headerConsumptionSelector',
            options: [{ value: '1600', label: '1 Person - 1600 kWh/Jahr ' }],
            onChange: jest.fn(),
            onBlur: jest.fn()
        };

        const component = renderComponent(propsMock);

        component.setState({ expanded: true });
        component
            .find('.lba-select-option')
            .at(0)
            .simulate('click');
        component.update();

        expect(propsMock.onChange).toHaveBeenCalledWith({ name: 'headerConsumptionSelector', value: '1600' });
        expect(propsMock.onBlur).toHaveBeenCalledWith({ target: { name: 'headerConsumptionSelector', value: '1600' } });
    });

    it('should remove event listener before component unmount', () => {
        jest.spyOn(document.body, 'removeEventListener');

        const component = renderComponent();

        component.unmount();
        expect(document.body.removeEventListener).toHaveBeenCalled();

        document.body.removeEventListener.mockRestore();
    });

    it('should render the option values instead of labels, if we do not provide it', () => {
        const propsMock = {
            options: [{ value: '1600' }, { value: '2400' }, { value: '3200' }]
        };

        const component = renderComponent(propsMock, mount);

        component.setState({ expanded: true });
        component.update();

        expect(
            component
                .find('.lba-select-option-content')
                .at(0)
                .text()
        ).toEqual('1600');
    });

    it('should handle enter key press on "ChevronDownIcon"', () => {
        const component = renderComponent();

        expect(component.state().expanded).toBe(false);

        component
            .find('.lba-select-chevron-down')
            .at(0)
            .simulate('keyup', { key: KEYBOARD_KEY_VALUES.ENTER });

        expect(component.state().expanded).toBe(true);
    });

    it('should handle click on "ChevronDownIcon"', () => {
        const component = renderComponent();

        expect(component.state().expanded).toBe(false);

        component
            .find('.lba-select-chevron-down')
            .at(0)
            .simulate('click');

        expect(component.state().expanded).toBe(true);
    });

    it('should call onChange when change value when input is editable', () => {
        const onChangeMock = jest.fn();
        const eventStub = { target: { name: 'test', value: 'some value' } };
        const component = renderComponent({ onChange: onChangeMock, editable: true });

        component
            .find('.lba-select')
            .at(0)
            .simulate('change', eventStub);

        expect(onChangeMock).toHaveBeenCalledWith({ name: 'test', value: 'some value' });
    });

    it('should render readonly input when it is not editable', () => {
        const component = renderComponent({ editable: false });

        expect(
            component
                .find('.lba-select')
                .at(0)
                .props('readonly')
        ).toBeTruthy();
    });

    it('should render enabled input when it is editable', () => {
        const component = renderComponent({ editable: true });

        expect(
            component
                .find('.lba-select')
                .at(0)
                .props().disabled
        ).toBeFalsy();
    });

    it("shouldn't call onChange when change value when input is not editable", () => {
        const onChangeMock = jest.fn();
        const eventStub = { target: { name: 'test', value: 'some value' } };
        const component = renderComponent({ editable: false });

        component
            .find('.lba-select')
            .at(0)
            .simulate('change', eventStub);

        expect(onChangeMock).toHaveBeenCalledTimes(0);
    });

    it('should render options with icons', () => {
        const component = renderComponent({ ...defaultProps, optionIcon: <svg /> });

        expect(component.find('svg')).toHaveLength(11);
        expect(component.find('.lba-select-option-icons')).toHaveLength(4);
        expect(component.find('.lba-select-option--icon')).toHaveLength(4);
    });

    it('should render select error', () => {
        const component = renderComponent({ ...defaultProps, error: 'Error' });

        expect(component.find('.lba-select-container--error')).toHaveLength(1);
        expect(component.find('Error')).toHaveLength(1);
        expect(component.find('Error').props()).toEqual({ hasIcon: true, children: 'Error' });
    });

    it('should add class when have children', () => {
        const component = renderComponent({ ...defaultProps, children: <svg /> });

        expect(component.find('.lba-select-container--children')).toHaveLength(1);
    });

    it('should should use value as selected value', () => {
        const component = renderComponent({ ...defaultProps, value: '1600', editable: true });

        expect(component.find('.lba-select').props().value).toBe('1600');
    });

    it('should should use label as selected value', () => {
        const component = renderComponent({
            options: [
                { value: '1600', label: '1 Person - 1600 kWh/Jahr' },
                { value: '2400', label: '2 Personen - 2400 kWh/Jahr' },
                { value: '3200', label: '3 Personen - 3200 kWh/Jahr' },
                { value: '4200', label: '4 Personen - 4200 kWh/Jahr' }
            ],
            value: '1600',
            editable: false
        });

        expect(component.find('.lba-select').props().value).toBe('1 Person - 1600 kWh/Jahr');
    });

    it('should call "onBlur" when blurred component', () => {
        const onBlurMock = jest.fn();
        const event = { target: { name: 'usage', value: '1700' } };
        const component = renderComponent({ onBlur: onBlurMock, name: 'usage', value: '1600', editable: true });

        component
            .find('.lba-select')
            .at(0)
            .simulate('blur', event);

        const [[firstArg]] = onBlurMock.mock.calls;

        expect(firstArg.target).toEqual({ name: 'usage', value: '1700' });

        component.setProps({
            value: '2400'
        });

        component
            .find('.lba-select')
            .at(0)
            .simulate('blur', event);

        const [[], [secondCallFirstArg]] = onBlurMock.mock.calls;

        expect(secondCallFirstArg.target).toEqual({ name: 'usage', value: '1700' });

        component
            .find('.lba-select')
            .at(0)
            .simulate('change', { target: { name: 'usage', value: '3200' } });

        component
            .find('.lba-select')
            .at(0)
            .simulate('blur', event);

        const [[], [], [thirdCallFirstArg]] = onBlurMock.mock.calls;

        expect(thirdCallFirstArg.target).toEqual({ name: 'usage', value: '1700' });
    });
});
