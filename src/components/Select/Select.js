import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { KEYBOARD_KEY_VALUES } from '../../constants';
import TextInput from '../TextInput';

import './Select.css';

class Select extends React.PureComponent {
    constructor(props) {
        super(props);
        this.wrapperRef = null;
        this.optionRef = null;

        this.state = {
            value: props.value || '',
            expanded: false
        };
    }

    componentDidMount() {
        document.body.addEventListener('click', e => this.handleOutsideClick(e));
    }

    componentDidUpdate(prevProps) {
        const { value } = this.props;

        if (prevProps.value !== value) {
            this.setState({ value });
        }
    }

    componentWillUnmount() {
        document.body.removeEventListener('click', e => this.handleOutsideClick(e));
    }

    handleOutsideClick(event) {
        const { expanded } = this.state;

        if (expanded && this.wrapperRef && !this.wrapperRef.contains(event.target)) {
            this.setState({ expanded: false });
        }
    }

    handleOptionClick(value) {
        const { onChange, onBlur, name } = this.props;

        this.setState({ value, expanded: false });
        onChange && onChange({ name, value });
        onBlur && onBlur({ target: { name, value } });
    }

    handleOptionKeyStroke(event, value) {
        const { onChange, onBlur, name, editable } = this.props;
        const { nextSibling, previousSibling } = event.target;

        if (editable && event.key === KEYBOARD_KEY_VALUES.ARROW_DOWN && nextSibling) {
            nextSibling.focus && nextSibling.focus();
            event.preventDefault();
        }

        if (editable && event.key === KEYBOARD_KEY_VALUES.ARROW_UP && previousSibling) {
            previousSibling.focus && previousSibling.focus();
            event.preventDefault();
        }

        if (event.key === KEYBOARD_KEY_VALUES.ENTER) {
            this.setState({ value, expanded: false });
            onChange && onChange({ name, value });
            onBlur && onBlur({ target: { name, value } });
        }
    }

    focusOptions(event) {
        const { editable, autoComplete } = this.props;
        const { expanded } = this.state;
        if (autoComplete && editable && event.key === KEYBOARD_KEY_VALUES.ARROW_DOWN && event.altKey) {
            this.setState({ expanded: !expanded });
            event.preventDefault();
        }

        if (autoComplete && editable && expanded && !event.altKey && event.key === KEYBOARD_KEY_VALUES.ARROW_DOWN) {
            this.optionRef && this.optionRef.focus && this.optionRef.focus();
            event.preventDefault();
        }
    }

    handleChange(event) {
        const { value, name } = event.target;
        const { onChange } = this.props;
        this.setState({ value });
        return onChange && onChange({ name, value });
    }

    renderPersonIcons(countOfPersons = 0) {
        const { optionIcon } = this.props;
        const icons = [...Array(countOfPersons).keys()].map(uniqueKey => ({ ...optionIcon, key: uniqueKey }));
        return <div className="lba-select-option-icons">{icons}</div>;
    }

    renderOptions(selectedOption) {
        const { options, optionIcon, editable, autoComplete } = this.props;
        const renderedOptions = [];

        for (let i = 0; i < options.length; i++) {
            const option = options[i];
            const label = option.label || option.value;
            const value = option.value;

            const { value: stateValue } = this.state;

            const shouldRender =
                autoComplete && editable
                    ? value && `${value || ''}`.toLocaleLowerCase().includes(`${stateValue || ''}`.toLocaleLowerCase())
                    : true;
            if (shouldRender) {
                const isSelected = selectedOption && selectedOption.value === value;

                const classes = classNames(
                    'lba-select-option',
                    isSelected && 'lba-select-option--selected',
                    optionIcon && 'lba-select-option--icon'
                );

                const tabIndex = renderedOptions.length;
                renderedOptions.push(
                    <li
                        ref={el => (tabIndex === 0 ? (this.optionRef = el) : undefined)}
                        key={i}
                        className={classes}
                        tabIndex={tabIndex}
                        role="option"
                        aria-selected={!!isSelected ? true : undefined}
                        onClick={() => this.handleOptionClick(value)}
                        onKeyUp={event => this.handleOptionKeyStroke(event, value)}
                    >
                        {optionIcon && this.renderPersonIcons(i + 1)}
                        <div className="lba-select-option-content">
                            <span>{label}</span>
                        </div>
                    </li>
                );
            }
        }

        return renderedOptions;
    }

    getSelectedOption() {
        const { options, value: initialValue } = this.props;
        const { value: stateValue } = this.state;

        return options.find(option => option.value === (stateValue || initialValue));
    }

    render() {
        const {
            props: {
                id,
                className,
                disabled,
                name,
                placeholder,
                children,
                editable,
                error,
                autoComplete,
                ...otherProps
            },
            state: { expanded, value }
        } = this;

        const listBoxId = `lba-listbox-${id}`;
        const listBoxControlId = `${listBoxId}-expand-button`;

        const classes = classNames({
            'lba-select--not-editable': !editable,
            'lba-select-container': true,
            'lba-select-container--disabled': !!disabled,
            'lba-select-container--error': !!error,
            'lba-select-container--expanded': expanded,
            'lba-select-container--children': !!children,
            [className]: !!className
        });

        const selectedOption = this.getSelectedOption();
        const label = (selectedOption && selectedOption.label) || '';

        const inputProps = {
            type: 'text',
            name,
            disabled: disabled,
            readOnly: !editable,
            value: editable ? value : label,
            placeholder,
            className: 'lba-select',
            onChange: event => editable && this.handleChange(event),
            onBlur: event => otherProps.onBlur && otherProps.onBlur(event),
            onKeyDown: event => {
                this.focusOptions(event);
                return editable && otherProps.onKeyDown && otherProps.onKeyDown(event);
            }
        };

        return (
            <div
                ref={wrapperRef => (this.wrapperRef = wrapperRef)}
                id={id}
                className={classes}
                tabIndex={disabled ? -1 : 0}
                aria-label={placeholder}
                onFocus={otherProps && otherProps.onFocus}
                onClick={() => !disabled && (!editable || autoComplete) && this.setState({ expanded: !expanded })}
            >
                {children}
                <input aria-label={placeholder} {...inputProps} />
                <ChevronDownIcon
                    id={listBoxControlId}
                    role="button"
                    aria-label="Show select options"
                    aria-controls={listBoxId}
                    aria-expanded={expanded ? true : undefined}
                    onClick={() => !disabled && editable && this.setState({ expanded: !expanded })}
                    onKeyUp={e => {
                        this.focusOptions(e);
                        return (
                            e.key === KEYBOARD_KEY_VALUES.ENTER && !disabled && this.setState({ expanded: !expanded })
                        );
                    }}
                />
                <ul
                    id={listBoxId}
                    className="lba-select-options"
                    role="listbox"
                    aria-hidden={expanded ? undefined : true}
                    aria-live="polite"
                    aria-labelledby={listBoxControlId}
                >
                    {this.renderOptions(selectedOption)}
                </ul>
                {error && <TextInput.Error hasIcon>{error}</TextInput.Error>}
            </div>
        );
    }
}

Select.propTypes = {
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    children: PropTypes.node,
    className: PropTypes.string,
    name: PropTypes.string,
    value: PropTypes.string,
    disabled: PropTypes.bool,
    autoComplete: PropTypes.bool,
    placeholder: PropTypes.string,
    options: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.string,
            label: PropTypes.string
        })
    ),
    editable: PropTypes.bool,
    optionIcon: PropTypes.node,
    error: PropTypes.string
};

Select.defaultProps = {
    id: Date.now(),
    disabled: false,
    options: [],
    placeholder: 'Select option',
    name: 'consumptionSelect',
    editable: false,
    autoComplete: false,
    optionIcon: null
};

export default Select;

export const ChevronDownIcon = props => (
    <svg
        {...props}
        xmlns="http://www.w3.org/2000/svg"
        width="14"
        height="9"
        viewBox="0 0 14 9"
        className="lba-select-chevron-down"
        tabIndex="0"
    >
        <path fill="none" fillRule="evenodd" stroke="#233647" strokeWidth="2" d="M1 1l6 6 6-6" />
    </svg>
);
