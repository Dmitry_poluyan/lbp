### Select component:

```jsx
const state = {
    name: 'customEnergySelector',
    options: [
        { value: '1600', label: '1 Person - 1600 kWh/Jahr ' },
        { value: '2400', label: '2 Personen - 2400 kWh/Jahr' },
        { value: '3200', label: '3 Personen - 3200 kWh/Jahr' },
        { value: '4200', label: '4 Personen - 4200 kWh/Jahr' },
    ]
};

<Select options={state.options} value={state.value} onChange={value => console.log(value)} name={state.name}/>
```

#### Default selected state:

```jsx
const state = {
    value: '4200',
    options: [
        { value: '1600', label: '1 Person - 1600 kWh/Jahr ' },
        { value: '2400', label: '2 Personen - 2400 kWh/Jahr' },
        { value: '3200', label: '3 Personen - 3200 kWh/Jahr' },
        { value: '4200', label: '4 Personen - 4200 kWh/Jahr' },
    ]
};

<Select options={state.options} value={state.value} onChange={value => console.log(value)}/>
```

#### Editable select with auto complete:

```jsx
const state = {
    value: '4200',
    options: [
        { value: '1600', label: '1 Person - 1600 kWh/Jahr ' },
        { value: '2400', label: '2 Personen - 2400 kWh/Jahr' },
        { value: '3200', label: '3 Personen - 3200 kWh/Jahr' },
        { value: '4200', label: '4 Personen - 4200 kWh/Jahr' },
    ]
};

<Select autoComplete editable options={state.options} value={state.value} onChange={value => console.log(value)}/>
```

#### Disable state:

```jsx
const state = {
    value: '1800',
    disabled: true,
    options: [
        { value: '1600', label: '1 Person - 1600 kWh/Jahr ' },
        { value: '2400', label: '2 Personen - 2400 kWh/Jahr' },
        { value: '3200', label: '3 Personen - 3200 kWh/Jahr' },
        { value: '4200', label: '4 Personen - 4200 kWh/Jahr' },
    ]
};

<Select options={state.options} value={state.value} onChange={value => console.log(value)} disabled={state.disabled}/>
```

#### With error:

```jsx
const state = {
    error: 'Error',
    options: [
        { value: '1600', label: '1 Person - 1600 kWh/Jahr ' },
        { value: '2400', label: '2 Personen - 2400 kWh/Jahr' },
        { value: '3200', label: '3 Personen - 3200 kWh/Jahr' },
        { value: '4200', label: '4 Personen - 4200 kWh/Jahr' },
    ]
};

<Select options={state.options} error={state.error} onChange={value => console.log(value)}/>
```

