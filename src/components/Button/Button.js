import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { KEYBOARD_KEY_VALUES } from '../../constants';

import './Button.css';

const Button = props => {
    const { className, disabled, children, onClick } = props;

    const classes = classNames({ 'lba-button': true, 'lba-button--disabled': disabled, className });

    return (
        <button
            tabIndex="0"
            className={classes}
            onClick={event => !disabled && onClick && onClick(event)}
            onKeyUp={e => e.key === KEYBOARD_KEY_VALUES.ENTER && !disabled && onClick(e)}
        >
            {children}
        </button>
    );
};

Button.propTypes = {
    className: PropTypes.string,
    disabled: PropTypes.bool,
    children: PropTypes.node,
    onClick: PropTypes.func
};

Button.defaultProps = {
    disabled: false
};

export default Button;
