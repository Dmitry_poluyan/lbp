#### Button component:

```jsx
<Button onClick={() => console.log('Hi!')}>Click</Button>
```

#### Disabled Button state:

```jsx
<Button disabled onClick={() => console.log('Hi!')}>Click</Button>
```
