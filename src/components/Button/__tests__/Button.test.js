import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import Button from '../Button';
import { KEYBOARD_KEY_VALUES } from '../../../constants';

function renderComponent(props = {}, mountFn = shallow) {
    return mountFn(<Button {...props}>Test</Button>);
}

describe('<Button /> Component', () => {
    it('should renders without errors', () => {
        const component = renderComponent();
        expect(component.hasClass('lba-button')).toBeTruthy();

        const dom = renderer.create(<Button onClick={f => f}>Test</Button>).toJSON();
        expect(dom).toMatchSnapshot();
    });

    it('should renders with success styles', () => {
        const component = renderComponent({ disabled: true });

        expect(component.hasClass('lba-button')).toBeTruthy();
        expect(component.hasClass('lba-button--disabled')).toBeTruthy();
    });

    it('should handle click event', () => {
        const onClickSub = jest.fn();
        const component = renderComponent({ onClick: onClickSub });

        component
            .find('button')
            .at(0)
            .simulate('click');
        expect(onClickSub).toHaveBeenCalled();
    });

    it("shouldn't handle click event when button is disabled", () => {
        const onClickSub = jest.fn();
        const component = renderComponent({ onClick: onClickSub, disabled: true });

        component
            .find('button')
            .at(0)
            .simulate('click');
        expect(onClickSub).toHaveBeenCalledTimes(0);
    });

    it('should handle enter key press event', () => {
        const onClickSub = jest.fn();
        const component = renderComponent({ onClick: onClickSub });

        component
            .find('button')
            .at(0)
            .simulate('keyup', { key: KEYBOARD_KEY_VALUES.ENTER });
        expect(onClickSub).toHaveBeenCalled();
    });

    it("shouldn't handle enter key press event when button is disabled", () => {
        const onClickSub = jest.fn();
        const component = renderComponent({ onClick: onClickSub, disabled: true });

        component
            .find('button')
            .at(0)
            .simulate('keyup', { key: KEYBOARD_KEY_VALUES.ENTER });
        expect(onClickSub).toHaveBeenCalledTimes(0);
    });
});
