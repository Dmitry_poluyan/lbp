import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

import Comment from '../Comment';
import StateIndicator from '../StateIndicator';

import './HistoryItem.css';

const HistoryItem = ({ className, state, date, time, amount, labels, comment, theme }) => {
    const classes = classNames(
        'lba-history-item',
        'lba-history-item2',
        theme && `lba-history-item--${theme}`,
        className
    );

    return (
        <div className={classes}>
            <section className="lba-history-state">
                <p>
                    {date} <span>{time}</span>
                </p>
                <StateIndicator state={state} theme={theme} />
                <p>{labels.eventTitle}</p>
            </section>
            {amount ? (
                <section className="lba-history-amount">
                    <p>{labels.amountTitle}</p>
                    <p>
                        {amount.prev} {labels.currency}
                        <i className="zmdi zmdi-chevron-right" />
                        {amount.new} {labels.currency}
                    </p>
                </section>
            ) : null}
            {comment ? (
                <section className="lba-history-comment">
                    <Comment author={comment.author} message={comment.message} theme={theme} />
                </section>
            ) : null}
        </div>
    );
};

HistoryItem.propTypes = {
    className: PropTypes.string,
    theme: PropTypes.oneOf(['dark']),
    state: PropTypes.oneOf(['review', 'waiting', 'rejected', 'approved']),
    date: PropTypes.string,
    time: PropTypes.string,
    labels: PropTypes.shape({
        eventTitle: PropTypes.string,
        amountTitle: PropTypes.string,
        currency: PropTypes.string
    }),
    amount: PropTypes.shape({
        prev: PropTypes.string,
        new: PropTypes.string
    }),
    comment: PropTypes.shape({
        author: PropTypes.string,
        message: PropTypes.string
    })
};

HistoryItem.defaultProps = {
    labels: {}
};

export default HistoryItem;
