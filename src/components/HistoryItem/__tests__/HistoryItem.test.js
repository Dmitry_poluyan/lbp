import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';

import HistoryItem from '../HistoryItem';

const defaultProps = {
    state: 'waiting',
    date: '1 March 2019',
    time: '5:01 PM',
    labels: {
        currency: '€',
        eventTitle: 'Application submitted'
    }
};

const amountProps = {
    amount: {
        prev: '50,000',
        new: '45,000'
    },
    labels: {
        currency: '€',
        eventTitle: 'Application submitted',
        amountTitle: 'Loan amount'
    }
};

const commentProps = {
    comment: {
        author: 'Michael Brown',
        message:
            "We think that it's difficult to afford a property of 50,000 € in Berlin with a down payment of 10,000 €."
    }
};

function renderComponent(props = {}, mountFn = shallow) {
    return mountFn(<HistoryItem {...defaultProps} {...props} />);
}

xdescribe('<HistoryItem /> Component', () => {
    it('should renders component', () => {
        const component = renderComponent();

        expect(component.hasClass('lba-history-item')).toBeTruthy();
        expect(component.find('.lba-history-state')).toHaveLength(1);
        expect(component.find('StateIndicator')).toHaveLength(1);
        expect(component.find('p')).toHaveLength(2);
        expect(component.find('.lba-history-amount')).toHaveLength(0);
        expect(component.find('.zmdi-chevron-right')).toHaveLength(0);
        expect(component.find('.lba-history-comment')).toHaveLength(0);
        expect(component.find('Comment')).toHaveLength(0);

        const dom = renderer.create(<HistoryItem {...defaultProps} {...amountProps} {...commentProps} />).toJSON();
        expect(dom).toMatchSnapshot();
    });

    it('should renders with correct props', () => {
        const component = renderComponent();

        expect(component.find('StateIndicator').props().state).toBe('waiting');
        expect(
            component
                .find('.lba-history-state p')
                .at(0)
                .text()
        ).toBe('1 March 2019 5:01 PM');
        expect(
            component
                .find('.lba-history-state p')
                .at(1)
                .text()
        ).toBe('Application submitted');
    });

    it('should renders amount with correct props', () => {
        const component = renderComponent({ ...defaultProps, ...amountProps });

        expect(component.find('.lba-history-amount')).toHaveLength(1);
        expect(component.find('.zmdi-chevron-right')).toHaveLength(1);
        expect(component.find('.lba-history-amount p')).toHaveLength(2);

        expect(
            component
                .find('.lba-history-amount p')
                .at(0)
                .text()
        ).toBe('Loan amount');
        expect(
            component
                .find('.lba-history-amount p')
                .at(1)
                .text()
        ).toBe('50,000 €45,000 €');
    });

    it('should renders comment with correct props', () => {
        const component = renderComponent({ ...defaultProps, ...commentProps });

        const comment = component.find('Comment');
        expect(comment).toHaveLength(1);
        expect(component.find('.lba-history-comment')).toHaveLength(1);

        expect(comment.props().author).toBe('Michael Brown');
        expect(comment.props().message).toBe(
            "We think that it's difficult to afford a property of 50,000 € in Berlin with a down payment of 10,000 €."
        );
    });

    it('should renders with dark theme', () => {
        const component = renderComponent({ ...defaultProps, ...commentProps, theme: 'dark' });
        expect(component.hasClass('lba-history-item')).toBeTruthy();
        expect(component.hasClass('lba-history-item--dark')).toBeTruthy();
        expect(component.find('Comment').props().theme).toBe('dark');
        expect(component.find('StateIndicator').props().theme).toBe('dark');
    });
});
