#### HistoryItem component:
```jsx
const wrapperStyles = {
    width: '335px',
    backgroundColor: 'rgba(112, 194, 255, 0.05)',
    padding: '20px'
};

const props = {
    state: 'waiting',
    date: '1 March 2019',
    time: '5:01 PM',
    labels: {
        currency: '€',
        eventTitle: 'Application submitted'
    }
};

<div style={wrapperStyles}>
    <HistoryItem {...props} />
</div>
```

#### HistoryItem component with amount section:

```jsx
const wrapperStyles = {
    width: '335px'
};

const props = {
    state: 'waiting',
    date: '1 March 2019',
    time: '5:01 PM',
    amount: {
        prev: '50,000',
        new: '45,000',
    },
    labels: {
        currency: '€',
        eventTitle: 'Application submitted',
        amountTitle: 'Loan amount'
    }
};

<div style={wrapperStyles}>
    <HistoryItem {...props} />
</div>
```

#### HistoryItem component with comment:

```jsx
const wrapperStyles = {
    width: '335px'
};

const props = {
    state: 'waiting',
    date: '1 March 2019',
    time: '5:01 PM',
    labels: {
        currency: '€',
        eventTitle: 'Application submitted',
        amountTitle: 'Loan amount'
    },
    comment: {
        author: 'Michael Brown',
        message: "We think that it's difficult to afford a property of 50,000 € in Berlin with a down payment of 10,000 €."
    }
};

<div style={wrapperStyles}>
    <HistoryItem {...props} />
</div>
```

#### HistoryItem component with amount and comment:

```jsx
const wrapperStyles = {
    width: '335px'
};

const props = {
    state: 'waiting',
    date: '1 March 2019',
    time: '5:01 PM',
    amount: {
        prev: '50,000',
        new: '45,000',
    },
    labels: {
        currency: '€',
        eventTitle: 'Application submitted',
        amountTitle: 'Loan amount'
    },
    comment: {
        author: 'Michael Brown',
        message: "We think that it's difficult to afford a property of 50,000 € in Berlin with a down payment of 10,000 €."
    }
};

<div style={wrapperStyles}>
    <HistoryItem {...props} />
</div>
```

#### HistoryItem component with amount and comment with dark theme:

```jsx
const wrapperStyles = {
    width: '335px'
};

const props = {
    state: 'waiting',
    date: '1 March 2019',
    time: '5:01 PM',
    amount: {
        prev: '50,000',
        new: '45,000',
    },
    labels: {
        currency: '€',
        eventTitle: 'Application submitted',
        amountTitle: 'Loan amount'
    },
    comment: {
        author: 'Michael Brown',
        message: "We think that it's difficult to afford a property of 50,000 € in Berlin with a down payment of 10,000 €."
    },
    theme: 'dark'
};

<div style={wrapperStyles}>
    <HistoryItem {...props} />
</div>
```
