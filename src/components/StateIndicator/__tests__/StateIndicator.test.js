import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';

import StateIndicator from '../StateIndicator';

function renderComponent(props = {}, mountFn = shallow) {
    return mountFn(<StateIndicator {...props} />);
}

describe('<StateIndicator /> Component', () => {
    it('should renders component', () => {
        const component = renderComponent();
        expect(component.hasClass('lba-state-indicator')).toBeTruthy();
        expect(component.find('.lba-indicator')).toHaveLength(1);

        const dom = renderer.create(<StateIndicator state="approved" />).toJSON();
        expect(dom).toMatchSnapshot();
    });

    it('should renders with default state', () => {
        const component = renderComponent();
        expect(component.hasClass('lba-state-indicator')).toBeTruthy();
        expect(component.hasClass('lba-state-indicator--waiting')).toBeTruthy();
        expect(component.find('p').text()).toBe('Waiting for review');
    });

    it('should renders with review state', () => {
        const component = renderComponent({ state: 'review' });
        expect(component.hasClass('lba-state-indicator')).toBeTruthy();
        expect(component.hasClass('lba-state-indicator--review')).toBeTruthy();
        expect(component.find('p').text()).toBe('In review');
    });

    it('should renders with review state', () => {
        const component = renderComponent({ state: 'rejected' });
        expect(component.hasClass('lba-state-indicator')).toBeTruthy();
        expect(component.hasClass('lba-state-indicator--rejected')).toBeTruthy();
        expect(component.find('p').text()).toBe('Rejected');
    });

    it('should renders with review state', () => {
        const component = renderComponent({ state: 'approved' });
        expect(component.hasClass('lba-state-indicator')).toBeTruthy();
        expect(component.hasClass('lba-state-indicator--approved')).toBeTruthy();
        expect(component.find('p').text()).toBe('Approved');
    });

    it('should renders with custom state labels', () => {
        const component = renderComponent({
            state: 'approved',
            labels: {
                review: 'In review state',
                waiting: 'Waiting for review state',
                rejected: 'Rejected state',
                approved: 'Approved state'
            }
        });
        expect(component.hasClass('lba-state-indicator')).toBeTruthy();
        expect(component.hasClass('lba-state-indicator--approved')).toBeTruthy();
        expect(component.find('p').text()).toBe('Approved state');
    });

    it('should renders with dark theme', () => {
        const component = renderComponent({ theme: 'dark' });
        expect(component.hasClass('lba-state-indicator')).toBeTruthy();
        expect(component.hasClass('lba-state-indicator--dark')).toBeTruthy();
    });
});
