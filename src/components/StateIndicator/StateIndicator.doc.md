#### StateIndicator in review state:

```jsx
<StateIndicator state="review" />
```

#### StateIndicator in waiting state:

```jsx
<StateIndicator state="waiting" />
```

#### StateIndicator in rejected state:

```jsx
<StateIndicator state="rejected" />
```

#### StateIndicator in approved state:

```jsx
<StateIndicator state="approved" />
```

#### StateIndicator in review state with dark theme:

```jsx
const wrapperStyles = {
    width: '125px',
    background: 'linear-gradient(0deg, #0066B3 0%, #3F98D4 100%)'
};

<div style={wrapperStyles}>
    <StateIndicator state="review" theme="dark" />
</div> 
```
