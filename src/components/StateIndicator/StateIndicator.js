import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './StateIndicator.css';

const StateIndicator = ({ className, state, labels, theme }) => {
    const classes = classNames(
        'lba-state-indicator',
        state && `lba-state-indicator--${state}`,
        theme && `lba-state-indicator--${theme}`,
        className
    );
    const label = labels[state];

    return (
        <div className={classes}>
            <div className="lba-indicator">
                <div />
            </div>
            <p>{label}</p>
        </div>
    );
};

StateIndicator.propTypes = {
    className: PropTypes.string,
    theme: PropTypes.oneOf(['dark']),
    state: PropTypes.oneOf(['review', 'waiting', 'rejected', 'approved']),
    labels: PropTypes.object
};

StateIndicator.defaultProps = {
    state: 'waiting',
    labels: {
        review: 'In review',
        waiting: 'Waiting for review',
        rejected: 'Rejected',
        approved: 'Approved'
    }
};

export default StateIndicator;
