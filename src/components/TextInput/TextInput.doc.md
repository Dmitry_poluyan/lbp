#### TextInput component:

```jsx
<TextInput value="12345 Berlin" onChange={(e) => console.log(e.target.value)} />
```

#### TextInput error state:

```jsx
<TextInput placeholder="Deine PLZ" onChange={(e) => console.log(e.target.value)} error="This field is required" />
```

#### TextInput disable state:

```jsx
<TextInput value="Minsk 23009" onChange={(e) => console.log(e.target.value)} disabled />
```
