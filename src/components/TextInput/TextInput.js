import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import InputMask from 'react-input-mask';

import './TextInput.css';

export default class TextInput extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = { value: null };
    }

    componentDidUpdate(prevProps) {
        const { value } = this.props;

        if (prevProps.value !== value) {
            this.setState({ value });
        }
    }

    handleChange(event) {
        const { onChange } = this.props;
        const { value } = event.target;

        this.setState({ value });
        return onChange && onChange(event);
    }

    render() {
        const {
            props: {
                className,
                value,
                disabled,
                error,
                label,
                name,
                children,
                placeholder,
                helperText,
                mask,
                maskChar,
                beforeMaskedValueChange,
                hasErrorIcon,
                ...otherProps
            },
            state: { value: stateValue }
        } = this;

        const classes = classNames({
            'lba-text-input': true,
            'lba-text-input--disabled': !!disabled,
            'lba-text-input--error': !!error,
            'lba-text-input--with-icon': !!children,
            [className]: !!className
        });

        const controlProps = {
            value: stateValue === undefined || stateValue === null ? value || '' : stateValue,
            name,
            placeholder,
            disabled,
            'aria-label': label || placeholder || (!!name ? `${name} Field` : 'Text Input'),
            ...otherProps
        };

        const Input = mask ? InputMask : 'input';

        // Specific props for MaskInput
        if (mask) {
            controlProps.mask = mask;
            controlProps.maskChar = maskChar;
            controlProps.beforeMaskedValueChange = beforeMaskedValueChange;
        }

        return (
            <div className={classes} onChange={event => this.handleChange(event)}>
                <Input {...controlProps} />
                {children}
                {helperText && <small className="lba-text-input-helper-text">{helperText}</small>}
                {error && <TextInput.Error hasIcon={hasErrorIcon}>{error}</TextInput.Error>}
            </div>
        );
    }
}

TextInput.propTypes = {
    className: PropTypes.string,
    children: PropTypes.node,
    error: PropTypes.string,
    value: PropTypes.string,
    label: PropTypes.string,
    placeholder: PropTypes.string,
    name: PropTypes.string,
    disabled: PropTypes.bool,
    helperText: PropTypes.string,
    hasErrorIcon: PropTypes.bool,
    mask: PropTypes.string,
    maskChar: PropTypes.any,
    beforeMaskedValueChange: PropTypes.func,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    onKeyDown: PropTypes.func,
    onChange: PropTypes.func
};

TextInput.defaultProps = {
    helperText: '',
    hasErrorIcon: true
};

TextInput.Error = function Error(props) {
    return (
        <div role="alert" className="lba-text-input-error-alert">
            {props.hasIcon && <i className="lba-text-input-warning-icon" aria-hidden />}
            <span>{props.children}</span>
        </div>
    );
};
