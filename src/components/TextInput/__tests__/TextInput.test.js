import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import InputMask from 'react-input-mask';

import TextInput from '../TextInput';

function renderComponent(props = {}, mountFn = shallow) {
    return mountFn(
        <TextInput {...props}>
            <svg />
        </TextInput>
    );
}

describe('<TextInput /> Component', () => {
    it('should renders without errors', () => {
        const component = renderComponent();
        expect(component.find('.lba-text-input input')).toHaveLength(1);
        expect(component.find('.lba-text-input svg')).toHaveLength(1);

        const dom = renderer
            .create(<TextInput placeholder="test" error="error" icon={props => <svg {...props} />} />)
            .toJSON();
        expect(dom).toMatchSnapshot();
    });

    it('should render with default `aria-label` when no label or placeholder were passed', () => {
        const textInput = renderComponent({ name: 'address' });

        expect(textInput.find('input').props()['aria-label']).toBe('address Field');
    });

    it('should update value stored in state when new value was passed in props', () => {
        const textInput = renderComponent();
        textInput.setState({ value: 'abc' });
        textInput.update();

        textInput.setProps({ value: 'xyz' });
        textInput.update();

        expect(textInput.state().value).toBe('xyz');
    });

    it('should show error alert in error cases', () => {
        const component = renderComponent();
        expect(component.find('div.lba-text-input-error-alert[role="alert"]')).toHaveLength(0);
        component.setProps({ error: 'This field is required' });
        component.update();
        expect(component.find('Error')).toHaveLength(1);
        expect(component.find('Error').props().children).toEqual('This field is required');
    });

    it('should provide possibility to enter text', () => {
        const onChangeStub = jest.fn();
        const defaultText = '12345 Berlin';
        const component = renderComponent({ value: defaultText, onChange: onChangeStub });
        expect(component.state().value).toEqual(null);
        expect(
            component
                .find('input')
                .at(0)
                .props().value
        ).toEqual(defaultText);
        component.simulate('change', { target: { value: 'q' } });
        component.update();
        expect(onChangeStub).toHaveBeenCalledWith({ target: { value: 'q' } });
        expect(component.state().value).toEqual('q');
        expect(
            component
                .find('input')
                .at(0)
                .props().value
        ).toEqual('q');
    });

    it('should show helper text if it was provided', () => {
        const component = renderComponent();
        expect(component.find('small.lba-text-input-helper-text')).toHaveLength(0);
        component.setProps({ helperText: 'This is helper text' });
        component.update();
        expect(component.find('small.lba-text-input-helper-text')).toHaveLength(1);
        expect(component.find('.lba-text-input-helper-text').text()).toEqual('This is helper text');
    });

    it('should render masked field', () => {
        const component = renderComponent({ mask: '99/99/9999' });
        expect(component.find(InputMask)).toHaveLength(1);
    });

    it('should not calls onFocus if onFocus is not a function', () => {
        const component = renderComponent({ onFocus: null });

        component.find('.lba-text-input input').simulate('focus');
    });

    it('should calls onBlur callback when field was blurred', () => {
        const onBlurStub = jest.fn();
        const component = renderComponent({ onBlur: onBlurStub });

        component.find('.lba-text-input input').simulate('blur');
        expect(onBlurStub).toHaveBeenCalled();
    });

    it('should not calls onBlur if onBlur is not a function', () => {
        const component = renderComponent({ onBlur: null });

        component.find('.lba-text-input input').simulate('blur');
    });

    it('should calls onChange callback when input value was changed', () => {
        const onChangeStub = jest.fn();
        const component = renderComponent({ onChange: onChangeStub });
        const eventDummy = {
            target: {
                value: 'test'
            }
        };

        component.find('.lba-text-input input').simulate('change', eventDummy);
        expect(onChangeStub).toHaveBeenCalledWith(eventDummy);
    });

    it('should not calls onChange if onChange is not a function', () => {
        const component = renderComponent({ onChange: null });
        const eventDummy = {
            target: {
                value: 'test'
            }
        };

        component.find('.lba-text-input input').simulate('change', eventDummy);
    });

    it('should calls onKeyDown callback', () => {
        const onKeyDownStub = jest.fn();
        const component = renderComponent({ onKeyDown: onKeyDownStub });
        const eventDummy = {
            key: 'Enter'
        };

        component.find('.lba-text-input input').simulate('keyDown', eventDummy);
        expect(onKeyDownStub).toHaveBeenCalledWith(eventDummy);
    });
});
