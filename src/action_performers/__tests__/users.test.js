import { dispatcher } from '../../store';

import { performRegistration, performLogin, performLogout, performGetUserData } from '../users';

describe('Users action performers', () => {
    beforeEach(() => {
        dispatcher.dispatchPromise = jest.fn();
    });

    it('should call dispatch method for user registration', () => {
        const userData = { firstName: 'John', lastName: 'Doe', username: 'test', password: 'qwerty123' };

        performRegistration(userData);

        const [firstCall] = dispatcher.dispatchPromise.mock.calls;
        const [method, type, loadingFunc, meta] = firstCall;
        const loading = loadingFunc({
            Users: { registration: { loading: 'TEST' } }
        });
        const [data] = meta;

        expect(dispatcher.dispatchPromise.mock.calls.length).toEqual(1);
        expect(method.name).toEqual('create');
        expect(type).toEqual('REGISTRATION');
        expect(loading).toEqual('TEST');
        expect(data).toEqual(userData);
    });

    it('should call dispatch method for user login', () => {
        performLogin({ username: 'test', password: 'test' });

        const [firstCall] = dispatcher.dispatchPromise.mock.calls;
        const [method, type, loadingFunc, meta] = firstCall;
        const loading = loadingFunc({
            Users: { login: { loading: 'TEST' } }
        });
        const [credentials] = meta;

        expect(dispatcher.dispatchPromise.mock.calls.length).toEqual(1);
        expect(method.name).toEqual('login');
        expect(type).toEqual('LOGIN');
        expect(loading).toEqual('TEST');
        expect(credentials).toEqual({ username: 'test', password: 'test' });
    });

    it('should call dispatch method for user logout', () => {
        performLogout();

        const [firstCall] = dispatcher.dispatchPromise.mock.calls;
        const [method, type, loadingFunc, meta] = firstCall;
        const loading = loadingFunc({
            Users: { logout: { loading: 'TEST' } }
        });

        expect(dispatcher.dispatchPromise.mock.calls.length).toEqual(1);
        expect(method.name).toEqual('logout');
        expect(type).toEqual('LOGOUT');
        expect(loading).toEqual('TEST');
        expect(meta).toEqual(undefined);
    });

    it('should call dispatch method for get user data', () => {
        performGetUserData();

        const [firstCall] = dispatcher.dispatchPromise.mock.calls;
        const [method, type, loadingFunc, meta] = firstCall;
        const loading = loadingFunc({
            Users: { profile: { loading: 'TEST' } }
        });

        expect(dispatcher.dispatchPromise.mock.calls.length).toEqual(1);
        expect(method.name).toEqual('getUserData');
        expect(type).toEqual('GET_USER_DATA');
        expect(loading).toEqual('TEST');
        expect(meta).toEqual(undefined);
    });
});
