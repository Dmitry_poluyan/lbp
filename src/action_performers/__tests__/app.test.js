import { dispatcher } from '../../store';
import { performSetupLoaderVisibility } from '../app';

describe('Users action performers', () => {
    beforeEach(() => {
        dispatcher.dispatchAction = jest.fn();
        dispatcher.dispatchPromise = jest.fn();
    });

    it('should call dispatch method for setting loader visibility', () => {
        const pageId = Symbol('pageId');
        performSetupLoaderVisibility(pageId, true);

        const [firstCall] = dispatcher.dispatchAction.mock.calls;
        const [type, payload] = firstCall;
        expect(dispatcher.dispatchAction).toHaveBeenCalledTimes(1);
        expect(type).toEqual('SETUP_LOADER_VISIBILITY');
        expect(payload).toEqual({ id: pageId, waiting: true });
    });
});
