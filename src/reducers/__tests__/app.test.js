import { appReducer, initialState } from '../app';

const { ACTIONS } = fixtures();

describe('App reducer:', () => {
    describe('Loader:', () => {
        it('should handle SETUP_LOADER_VISIBILITY', () => {
            const result = appReducer(initialState, ACTIONS.setupLoaderVisibility);
            const { payload } = ACTIONS.setupLoaderVisibility;
            expect(result.loader.data).toEqual({ [payload.id]: payload.waiting });
        });
    });
});

function fixtures() {
    const ACTIONS = {
        setupLoaderVisibility: {
            type: 'SETUP_LOADER_VISIBILITY',
            payload: { id: 'PAGE_NAME', waiting: true }
        }
    };
    return { ACTIONS };
}
