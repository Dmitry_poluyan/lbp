import { combineReducers } from 'redux';
import { usersReducer } from './users';
import { appReducer } from './app';

const reducers = combineReducers({
    Users: usersReducer,
    App: appReducer
});

export default reducers;
