import React from 'react';
import { shallow } from 'enzyme';

import NotFoundPage from '../NotFoundPage';

function renderComponent(mountFn = shallow) {
    return mountFn(<NotFoundPage />);
}

describe('<NotFoundPage /> Component', () => {
    it(`should contains following controls:
        - 1 <h1> with correct text;`, () => {
        const component = renderComponent();

        expect(component.find('h1')).toHaveLength(1);
        expect(component.find('h1').text()).toBe('Page Not Found');
    });
});
