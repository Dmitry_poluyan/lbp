import React from 'react';

import './NotFoundPage.css';

export class NotFoundPage extends React.PureComponent {
    render() {
        return (
            <div className="lba-not-found-page">
                <h1>Page Not Found</h1>
            </div>
        );
    }
}

export default NotFoundPage;
