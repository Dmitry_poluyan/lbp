import React from 'react';
import { shallow } from 'enzyme';

import { LoanAppDetails } from '../LoanAppDetails';

function renderComponent(props = {}) {
    return shallow(<LoanAppDetails {...props} />);
}

describe('<LoanAppDetails /> Component', () => {
    it(`should render with correct elements`, () => {
        const component = renderComponent();

        expect(component.find('div')).toHaveLength(1);
        expect(component.find('h1')).toHaveLength(1);
    });
});
