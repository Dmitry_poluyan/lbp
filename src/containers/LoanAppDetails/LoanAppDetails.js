import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button, HistoryItem } from '../../components';
import { performGetUserData } from '../../action_performers/users';
import { performSetupLoaderVisibility } from '../../action_performers/app';
import AppPage from '../__shared__/AppPage';

import './LoanAppDetails.css';

export class LoanAppDetails extends AppPage {
    static mapStateToProps(state) {
        return {
            registration: state.Users.registration.data,
            loading: state.Users.registration.loading || state.Users.profile.loading,
            error: state.Users.registration.error
        };
    }

    constructor(props, context) {
        super(props, context);
        this.state = {};

        this.openSignUpPage = this.openSignUpPage.bind(this);
    }

    componentDidMount() {
        performGetUserData();
    }

    componentDidUpdate({ loading }) {
        if (loading !== this.props.loading) {
            performSetupLoaderVisibility(this.pageId, this.props.loading);
        }
    }

    navigateTo(route) {
        this.context.router.history.push(route);
    }

    openSignUpPage() {
        this.navigateTo('/sign-up');
    }

    render() {
        const { registration } = this.props;
        const props = {
            state: 'waiting',
            date: '1 March 2019',
            time: '5:01 PM',
            amount: {
                prev: '50,000',
                new: '45,000'
            },
            labels: {
                currency: '€',
                eventTitle: 'Application submitted',
                amountTitle: 'Loan amount'
            },
            comment: {
                author: 'Michael Brown',
                message:
                    "We think that it's difficult to afford a property of 50,000 € in Berlin with a down payment of 10,000 €."
            }
        };

        return (
            <div className="lba-lon-app-details-page">
                <h1>LoanAppDetails</h1>
                <p>
                    User name: {registration.firstName} {registration.lastName}
                </p>
                <Button onClick={this.openSignUpPage}>Go to sign up</Button>
                <HistoryItem {...props} />
            </div>
        );
    }
}

LoanAppDetails.contextTypes = {
    router: PropTypes.shape({
        history: PropTypes.shape({
            push: PropTypes.func.isRequired
        }).isRequired
    })
};

LoanAppDetails.propTypes = {
    loading: PropTypes.bool,
    registration: PropTypes.object,
    error: PropTypes.object
};

LoanAppDetails.defaultProps = {
    loading: false,
    registration: {},
    error: null
};

export default connect(LoanAppDetails.mapStateToProps)(LoanAppDetails);
