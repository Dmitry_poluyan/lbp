import React from 'react';
import { shallow } from 'enzyme';

import { SignUp } from '../SignUp';

function renderComponent(props = {}) {
    return shallow(<SignUp {...props} />);
}

describe('<SignUp /> Component', () => {
    it(`should render with correct elements`, () => {
        const component = renderComponent();

        expect(component.find('div')).toHaveLength(1);
        expect(component.find('h1')).toHaveLength(1);
    });
});
