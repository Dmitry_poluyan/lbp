import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button } from '../../components';
import AppPage from '../../containers/__shared__/AppPage';
import { performSetupLoaderVisibility } from '../../action_performers/app';
import { performRegistration } from '../../action_performers/users';

import './SignUp.css';

export class SignUp extends AppPage {
    static mapStateToProps(state) {
        return {
            loading: state.Users.registration.loading,
            registration: state.Users.registration.data,
            error: state.Users.registration.error
        };
    }

    constructor(props, context) {
        super(props, context);
        this.state = {
            firstName: ''
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    componentDidUpdate(prevProps) {
        const { loading, registration } = this.props;
        const loaded = prevProps.loading !== loading && !loading;

        if (loaded && registration !== prevProps.registration) {
            this.openMainPage();
        }

        if (prevProps.loading !== loading) {
            performSetupLoaderVisibility(this.pageId, loading);
        }
    }

    openMainPage() {
        const { history } = this.context.router;
        history.push('/');
    }

    handleSubmit() {
        performRegistration({ ...this.state });
    }

    handleInputChange(event) {
        this.setState({ [event.target.name]: event.target.value });
    }

    render() {
        return (
            <div className="lba-sign-up-container">
                <h1>Sign up</h1>
                First name
                <input type="text" name="firstName" onChange={this.handleInputChange} />
                <Button onClick={this.handleSubmit}>Go to sign up</Button>
            </div>
        );
    }
}

SignUp.contextTypes = {
    router: PropTypes.shape({
        history: PropTypes.shape({
            push: PropTypes.func.isRequired
        }).isRequired
    })
};

SignUp.propTypes = {};

SignUp.defaultProps = {};

export default connect(SignUp.mapStateToProps)(SignUp);
