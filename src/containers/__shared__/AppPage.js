import React from 'react';

const PAGE_ID = Symbol('page-id');
const Base = React.Component;

class AppPage extends Base {
    constructor(props, context) {
        super(props, context);
        this.pageId = PAGE_ID;
    }
}

export default AppPage;
