import React from 'react';
import { shallow } from 'enzyme';

import { App } from '../App';

function renderComponent(props = {}) {
    return shallow(<App {...props} />);
}

describe('Main <App /> Component', () => {
    it(`should render with correct elements`, () => {
        const component = renderComponent();

        expect(component.find('div')).toHaveLength(3);
    });
});
