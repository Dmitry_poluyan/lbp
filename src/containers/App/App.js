import React from 'react';
import { connect } from 'react-redux';

import './App.css';

export class App extends React.PureComponent {
    static mapStateToProps() {
        return {};
    }

    constructor(props, context) {
        super(props, context);
        this.state = {};
    }

    render() {
        return (
            <div className="lba-app">
                <div className="lba-content">
                    <div role="article" id="lba-main-container">
                        <main>{this.props.children}</main>
                    </div>
                </div>
            </div>
        );
    }
}

App.propTypes = {};

App.defaultProps = {};

export default connect(App.mapStateToProps)(App);
